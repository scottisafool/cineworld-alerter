﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Extensions;
using CineworldAlerter.Core.Extensions;
using CineworldAlerter.Core.Services;
using CineworldAlerter.ViewModels.Entities;
using GalaSoft.MvvmLight;

namespace CineworldAlerter.ViewModels
{
    public class BookByDateViewModel : ViewModelBase
    {
        private readonly IBookingService _bookingService;
        private readonly ICinemaService _cinemaService;
        private readonly IFilmService _filmService;
        private readonly IToastService _toastService;
        private readonly Func<BookingByTimeViewModel> _bookingByTimeViewModelFunc;

        private Dictionary<string, FilmViewModel> _filmsById = new Dictionary<string, FilmViewModel>();

        private DateTimeOffset _selectedDate = DateTimeOffset.Now.Date;
        private bool _isLoading;
        private bool _allowForAdsAndTrailers = true;

        private bool _filteringIncludesSameFilm = true;
        private List<BookingByTimeViewModel> _allBookings = new List<BookingByTimeViewModel>();

        public bool HasFiltersApplied => HasChosenBookings || Films.Any(x => x.IsFiltered);
        public bool HasChosenBookings => ChosenBookings.Any();
        public bool FilmPickerIsVisible => _allBookings.Any();
        
        public string FilteredFilmsCount
        {
            get
            {
                var count = Films.Count(x => x.IsFiltered);

                switch (count)
                {
                    case 0:
                        return "Filter by film";
                    case 1:
                        return "1 film";
                    default:
                        return $"{count} films";
                }
            }
        }

        public bool FilteringIncludesSameFilm
        {
            get => _filteringIncludesSameFilm;
            set => Set(ref _filteringIncludesSameFilm, value);
        }

        public bool IsLoading
        {
            get => _isLoading;
            set => Set(ref _isLoading, value);
        }

        public bool AllowForAdsAndTrailers
        {
            get => _allowForAdsAndTrailers;
            set
            {
                if(Set(ref _allowForAdsAndTrailers, value))
                    ResetFiltering();
            }
        }


        public DateTimeOffset SelectedDate
        {
            get => _selectedDate;
            set
            {
                if (Set(ref _selectedDate, value))
                    LoadBookingsByDate().DontAwait();
            }
        }

        public ObservableCollection<BookingByTimeViewModel> FilteredBookings { get; } = new ObservableCollection<BookingByTimeViewModel>();
        
        public ObservableCollection<BookingByTimeViewModel> ChosenBookings { get; } = new ObservableCollection<BookingByTimeViewModel>();

        public ObservableCollection<FilmFilterViewModel> Films { get; } = new ObservableCollection<FilmFilterViewModel>();

        public BookByDateViewModel(
            IBookingService bookingService,
            ICinemaService cinemaService,
            IFilmService filmService,
            IToastService toastService,
            Func<BookingByTimeViewModel> bookingByTimeViewModelFunc)
        {
            _bookingService = bookingService;
            _cinemaService = cinemaService;
            _filmService = filmService;
            _toastService = toastService;
            _bookingByTimeViewModelFunc = bookingByTimeViewModelFunc;

            Initialise().DontAwait();

            ChosenBookings.CollectionChanged += FilterCollectionChanged;
            FilteredBookings.CollectionChanged += FilterCollectionChanged;
        }

        private void FilterCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            RaisePropertyChanged(nameof(HasFiltersApplied));
            RaisePropertyChanged(nameof(HasChosenBookings));
        }

        public void LoadBookingsForToday() 
            => LoadBookingsForFixedDate(DateTimeOffset.Now.Date);

        public void LoadBookingsForTomorrow() 
            => LoadBookingsForFixedDate(DateTimeOffset.Now.AddDays(1));

        private void LoadBookingsForFixedDate(DateTimeOffset date)
        {
            if (SelectedDate == date)
            {
                LoadBookingsByDate().DontAwait();
            }
            else
            {
                SelectedDate = date;
            }
        }

        public void ResetFiltering()
            => ResetTheFiltering(true);

        private void ResetTheFiltering(bool resetFilmList)
        {
            ChosenBookings.Clear();
            FilteredBookings.Clear();
            FilteredBookings.AddRange(_allBookings);

            foreach (var film in Films)
                film.IsFiltered = false;

            if (resetFilmList)
                CreateFilmList();

            RaisePropertyChanged(nameof(FilmPickerIsVisible));
            RaisePropertyChanged(nameof(FilteredFilmsCount));
        }

        private void LoadBookingsBasedOnExistingChosenBookings()
        {
            var updatedBookings = ChosenBookings
                .OrderBy(x => x.BookingStartTime)
                .ToList();

            FilterByFilm();

            foreach(var booking in updatedBookings)
                FilterBasedOnBooking(booking);
        }

        private async Task LoadBookingsByDate()
        {
            IsLoading = true;
            try
            {
                FilteredBookings.Clear();

                var bookings = await _bookingService.GetAllBookingsForDate(_cinemaService.CurrentCinema.Id, SelectedDate);

                _allBookings = bookings
                    .Where(x => _filmsById.ContainsKey(x.FilmId))
                    .Select(x => _bookingByTimeViewModelFunc().WithBooking(
                        x, 
                        _filmsById[x.FilmId], 
                        y => FilterBasedOnBooking(y),
                        DeleteBooking))
                    .ToList();

                CreateFilmList();

                FilterByFilm();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                IsLoading = false;
            }
        }

        private void CreateFilmList()
        {
            Films.Clear();

            var filmIds = _allBookings
                .Select(x => x.Film.Id)
                .Distinct();

            var films = filmIds
                .Select(x => _filmsById[x])
                .OrderBy(x => x.Name)
                .Select(x => new FilmFilterViewModel(FilterByFilm, x));

            Films.AddRange(films);
        }

        private void DeleteBooking(BookingByTimeViewModel booking)
        {
            ChosenBookings.Remove(booking);
            
            LoadBookingsBasedOnExistingChosenBookings();
        }

        private void FilterByFilm()
        {
            var filmsToHide = Films
                .Where(x => x.IsFiltered)
                .ToList();

            if (filmsToHide.IsNullOrEmpty())
            {
                ResetTheFiltering(false);
            }
            else
            {
                var bookingsToShow = _allBookings
                    .Where(x => filmsToHide.Any(y => y.Film.Id == x.Film.Id))
                    .ToList();

                FilteredBookings.Clear();
                FilteredBookings.AddRange(bookingsToShow);
            }

            RaisePropertyChanged(nameof(FilteredFilmsCount));
        }

        private void FilterBasedOnBooking(
            BookingByTimeViewModel bookingToFilterBy,
            bool canRemovePreviousBookings = true)
        {
            if (!ChosenBookings.Contains(bookingToFilterBy))
                ChosenBookings.Add(bookingToFilterBy);

            var runtime = bookingToFilterBy.Film.FilmLength;
            var offset = AllowForAdsAndTrailers ? BookingByTimeViewModel.AdsAndTrailerAllowanceInMinutes : 0;
            var totalFilmTime = runtime + offset;

            var finishTime = bookingToFilterBy.BookingStartTime.AddMinutes(totalFilmTime);

            var bookingsToHide = FilteredBookings
                .Where(x => 
                    FilterByStartTime(bookingToFilterBy, x, offset, finishTime) ||
                    (canRemovePreviousBookings && FilterOnPreviousOrEarlierBookings(bookingToFilterBy, x)) ||
                    IsSameFilm(x, bookingToFilterBy))
                .ToList();

            foreach (var booking in bookingsToHide)
                FilteredBookings.Remove(booking);

            RaisePropertyChanged(nameof(FilmPickerIsVisible));
        }

        private bool FilterOnPreviousOrEarlierBookings(
            BookingByTimeViewModel booking, 
            BookingByTimeViewModel bookingToCheck)
        {
            return bookingToCheck.BookingStartTime < booking.BookingStartTime
                   && !ChosenBookings.Contains(bookingToCheck);
        }

        private static bool FilterByStartTime(
            BookingByTimeViewModel booking, 
            BookingByTimeViewModel bookingToCheck, 
            int offset,
            DateTimeOffset finishTime)
        {
            return bookingToCheck.BookingStartTime >= booking.BookingStartTime 
                   && bookingToCheck.BookingStartTime.AddMinutes(offset) <= finishTime
                   && bookingToCheck != booking;
        }

        private bool IsSameFilm(
            BookingByTimeViewModel bookingToCheck, 
            BookingByTimeViewModel filteredBooking)
        {
            if (!FilteringIncludesSameFilm)
                return false;

            if (bookingToCheck.Film.Name == filteredBooking.Film.Name)
            {
                return bookingToCheck.BookingId != filteredBooking.BookingId;
            }

            return false;
        }

        private async Task Initialise()
        {
            IsLoading = true;

            try
            {
                var films = await _filmService.GetLocalFilms();
                _filmsById = films
                    .Where(x => _toastService.CanFilmBeDisplayed(x))
                    .ToDictionary(
                        x => x.Id,
                        x => new FilmViewModel(x));
            }
            catch
            {
            }
            finally
            {
                IsLoading = false;
            }
        }
    }
}
