﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Extensions;
using Cimbalino.Toolkit.Handlers;
using Cimbalino.Toolkit.Services;
using Cineworld.Api.Model;
using CineworldAlerter.Core.Services;
using GalaSoft.MvvmLight;

namespace CineworldAlerter.ViewModels
{
    public class StartupViewModel : ViewModelBase, IHandleNavigatedTo
    {
        private readonly ICineworldNavigationService _navigationService;
        private readonly ICinemaService _cinemaService;
        private readonly IUserPreferencesService _userPreferencesService;

        private Cinema _selectedCinema;
        private bool _displayNotifications;

        public ObservableCollection<Cinema> Cinemas { get; set; }
            = new ObservableCollection<Cinema>();

        public Cinema SelectedCinema
        {
            get => _selectedCinema;
            set
            {
                if(Set(ref _selectedCinema, value))
                    RaisePropertyChanged(nameof(CanMoveOn));
            }
        }

        public bool DisplayNotifications
        {
            get => _displayNotifications;
            set => Set(ref _displayNotifications, value);
        }

        public bool CanMoveOn => SelectedCinema != null;

        public StartupViewModel(
            ICineworldNavigationService navigationService,
            ICinemaService cinemaService,
            IUserPreferencesService userPreferencesService)
        {
            _navigationService = navigationService;
            _cinemaService = cinemaService;
            _userPreferencesService = userPreferencesService;
        }

        public async Task OnNavigatedToAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            if (_cinemaService.CurrentCinema != null)
            {
                await Task.Delay(10);

                _navigationService.NavigateToMainPage();
                _navigationService.ClearBackStack();
                return;
            }

            Cinemas.Clear();

            var cinemas = await _cinemaService.GetCurrentCinemas();
            Cinemas.AddRange(cinemas);

            DisplayNotifications = _userPreferencesService.DisplayNotifications;
        }

        public void SetCinema()
        {
            _cinemaService.ChangeCinema(SelectedCinema);

            _userPreferencesService.DisplayNotifications = DisplayNotifications;
            _userPreferencesService.Save();

            _navigationService.NavigateToMainPage();
            _navigationService.ClearBackStack();
        }
    }
}
