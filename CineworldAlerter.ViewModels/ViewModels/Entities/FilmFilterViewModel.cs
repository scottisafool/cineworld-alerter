﻿using System;
using GalaSoft.MvvmLight;

namespace CineworldAlerter.ViewModels.Entities
{
    public class FilmFilterViewModel : ViewModelBase
    {
        private readonly Action _filterAction;

        private bool _isFiltered;

        public bool IsFiltered
        {
            get => _isFiltered;
            set
            {
                if (Set(ref _isFiltered, value))
                    Filter();
            }
        }

        public FilmViewModel Film { get; }


        public FilmFilterViewModel(
            Action filterAction,
            FilmViewModel film)
        {
            Film = film;
            _filterAction = filterAction;
        }

        public void Filter()
            => _filterAction?.Invoke();
    }
}