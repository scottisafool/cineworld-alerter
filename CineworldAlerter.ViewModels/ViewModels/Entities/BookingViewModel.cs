﻿using System;
using System.Linq;
using Cimbalino.Toolkit.Services;
using Cineworld.Api.Model;
using CineworldAlerter.Core.Extensions;
using GalaSoft.MvvmLight;

namespace CineworldAlerter.ViewModels.Entities
{
    public class BookingViewModel : ViewModelBase
    {
        private readonly ILauncherService _launcherService;

        protected Booking Booking;

        public string BookingTime => FormatTime(Booking.BookingTime.TimeOfDay);

        public bool IsSoldOut => Booking.IsSoldOut;

        public bool Is3D => Booking.AttributeIds.Contains("3D") || Booking.AttributeIds.Contains("3d");

        public bool IsSubtitled => Booking.AttributeIds?.Contains("subbed") ?? false;

        public string Auditorium => FormatAuditorium();

        public BookingViewModel(
            ILauncherService launcherService) 
            => _launcherService = launcherService;

        public void LaunchBooking()
            => _launcherService.LaunchUriAsync(Booking.BookingLink.ToCineworldLink());

        public BookingViewModel WithBooking(Booking booking)
        {
            Booking = booking;
            return this;
        }

        protected string FormatTime(TimeSpan time)
            => time.ToString(@"hh\:mm");

        private string FormatAuditorium()
        {
            if (Booking.CinemaId != "8112")
            {
                return Booking.Auditorium;
            }

            switch (Booking.Auditorium)
            {
                case "Screen 11":
                    return "Studio A";
                case "Screen 12":
                    return "Studio B";
                case "Screen 13":
                    return "Studio C";
                case "Screen 14":
                    return "Studio D";
                case "Screen 15":
                    return "Studio E";
                case "Screen 16":
                    return "Studio F";
                default:
                    return Booking.Auditorium;
            }
        }
    }
}