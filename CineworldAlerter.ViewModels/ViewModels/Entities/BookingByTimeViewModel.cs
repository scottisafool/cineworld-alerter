﻿using System;
using System.Diagnostics;
using Cimbalino.Toolkit.Services;
using Cineworld.Api.Model;

namespace CineworldAlerter.ViewModels.Entities
{
    [DebuggerDisplay("Time: {BookingTime}")]
    public class BookingByTimeViewModel : BookingViewModel
    {
        public const int AdsAndTrailerAllowanceInMinutes = 20;

        private Action<BookingByTimeViewModel> _filterAction;
        private Action<BookingByTimeViewModel> _deleteAction;

        private bool _isInPlanner;

        public bool IsInPlanner
        {
            get => _isInPlanner;
            set => Set(ref _isInPlanner, value);
        }

        public BookingByTimeViewModel(
            ILauncherService launcherService) 
            : base(launcherService)
        {
        }

        public FilmViewModel Film { get; private set; }

        public DateTimeOffset BookingStartTime => Booking.BookingTime;

        public string NameAndRunTime => $"{Film.Name} ({Film.RunTime})";

        public string BookingId => Booking.Id;

        public string FinishTime
        {
            get
            {
                var runTime = Film.FilmLength;
                var totalFilmTime = runTime + AdsAndTrailerAllowanceInMinutes;

                var finishTime = BookingStartTime.AddMinutes(totalFilmTime);
                return $"Finish time: {FormatTime(finishTime.TimeOfDay)}";
            }
        }

        public BookingByTimeViewModel WithBooking(
            Booking booking,
            FilmViewModel film,
            Action<BookingByTimeViewModel> filterAction,
            Action<BookingByTimeViewModel> deleteAction)
        {
            Booking = booking;
            Film = film;
            _filterAction = filterAction;
            _deleteAction = deleteAction;
            return this;
        }

        public void FilterUsingThisBooking()
        {
            _filterAction?.Invoke(this);
            IsInPlanner = true;
        }

        public void DeleteThisBooking()
        {
            _deleteAction?.Invoke(this);
            IsInPlanner = false;
        }
    }
}
