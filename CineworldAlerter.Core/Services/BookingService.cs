﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cineworld.Api;
using Cineworld.Api.Model;

namespace CineworldAlerter.Core.Services
{
    public class BookingService: IBookingService
    {
        private readonly ICachingService<(string cinema, string film), List<DateTimeOffset>> _dateCachingService;
        private readonly ICachingService<(string cinema, string film, DateTimeOffset date), List<Booking>> _bookingCachingService;
        private readonly ICachingService<(string cinema, DateTimeOffset date), List<Booking>> _bookingByDateCachingService;

        public BookingService(
            ICachingService<(string cinema, string film), List<DateTimeOffset>> dateCachingService,
            ICachingService<(string cinema, string film, DateTimeOffset date), List<Booking>> bookingCachingService,
            ICachingService<(string cinema, DateTimeOffset date), List<Booking>> bookingByDateCachingService,
            IApiClient apiClient)
        {
            _dateCachingService = dateCachingService;
            _bookingCachingService = bookingCachingService;
            _bookingByDateCachingService = bookingByDateCachingService;

            _dateCachingService.Initialise(details => apiClient.GetDatesForFilmsByCinema(details.cinema, details.film));
            _bookingCachingService.Initialise(details => apiClient.GetBookings(details.cinema, details.film, details.date));
            _bookingByDateCachingService.Initialise(details => apiClient.GetAllBookingsForDate(details.cinema, details.date));
        }

        public Task<List<DateTimeOffset>> GetDates(string cinemaId, string filmId) 
            => _dateCachingService.Get((cinemaId, filmId));

        public Task<List<Booking>> GetBookings(string cinemaId, string filmId, DateTimeOffset screeningDate)
            => _bookingCachingService.Get((cinemaId, filmId, screeningDate));

        public Task<List<Booking>> GetAllBookingsForDate(string cinemaId, DateTimeOffset date) 
            => _bookingByDateCachingService.Get((cinemaId, date));
    }
}