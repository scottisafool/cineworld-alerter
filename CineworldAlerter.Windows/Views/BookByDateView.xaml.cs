﻿using System;
using System.Linq;
using Windows.UI.Xaml.Controls;
using CineworldAlerter.ViewModels;

namespace CineworldAlerter.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BookByDateView
    {
        private BookByDateViewModel ViewModel => DataContext as BookByDateViewModel;
        public BookByDateView()
        {
            InitializeComponent();

            Calendar.MinDate = DateTimeOffset.Now;
            Calendar.SetDisplayDate(DateTimeOffset.Now);
        }

        private void CalendarView_OnSelectedDatesChanged(CalendarView sender, CalendarViewSelectedDatesChangedEventArgs args)
        {
            var selectedDate = args.AddedDates.FirstOrDefault();
            if (selectedDate == default) return;

            ViewModel.SelectedDate = selectedDate;

            CalendarFlyout.Hide();
        }
    }
}
