﻿using CineworldAlerter.ViewModels.Entities;

namespace CineworldAlerter.Controls
{
    public sealed partial class BookingButton 
    {
        private BookingViewModel ViewModel => DataContext as BookingViewModel;
        public BookingButton()
        {
            InitializeComponent();

            DataContextChanged += (sender, args) => Bindings.Update();
        }
    }
}
